package com.milton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrlShorLenerApplication {
	private static final Logger logger = LoggerFactory.getLogger(UrlShorLenerApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(UrlShorLenerApplication.class, args);
		System.out.println("Url ShortLener");
		logger.info("Url ShortLener demo project compiled successfully ......");
	}


}
