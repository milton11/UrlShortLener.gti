package com.milton.controller;

import com.milton.dto.CustomResponse;
import com.milton.dto.Response;
import com.milton.dto.URLDto;
import com.milton.entity.Info;
import com.milton.entity.ShortUrl;
import com.milton.entity.URL;
import com.milton.repository.InfoRepository;
import com.milton.repository.ShortUrlRepository;
import com.milton.repository.UrlRepository;
import com.milton.service.InfoService;
import com.milton.service.UrlService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.List;

@AllArgsConstructor
//@RestController
@Controller
@RequestMapping("/api")
public class UrlShortLenerController {
private static final Logger logger = LoggerFactory.getLogger(UrlShortLenerController.class);
    private final UrlService urlService;
    private final UrlRepository urlRepository;
    private final InfoRepository infoRepository;
    private final ShortUrlRepository shortUrlRepository;

    @GetMapping("/thymeleaf")
    public String getUrl(Model model){
        List<URL>urls=urlService.getAllUrl();
        System.out.println(urls);
        model.addAttribute("url",urls);
    return "index";
}





    @GetMapping("/{id}")
    public ResponseEntity<URL>getById(@PathVariable Long id){
    URL url = urlService.findById(id);
        return ResponseEntity.ok(url);
    }

    @GetMapping("/all")
    public ResponseEntity<List<URL>> getAll(){
        List<URL>allUrl=urlService.getAllUrl();
        return new ResponseEntity<List<URL>>(allUrl,HttpStatus.OK);
    }


// endpoint which will redirect from shortUrl to long url
    @GetMapping("/url/{shortUrl}")
    public ResponseEntity<?>getUrl(@PathVariable String shortUrl,HttpServletRequest  request) throws URISyntaxException, UnknownHostException {
        URL url = urlService.getUrl(shortUrl);
        System.out.println("Fetching url from database: "+url);
        URI uri = new URI(url.getLongUrl());
        //URI uri = new URI("http://www.google.com");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(uri);
        System.out.print(uri);
        return new ResponseEntity<>(httpHeaders, HttpStatus.MOVED_PERMANENTLY);
    }

    //endpoint for creating url and store into db
    @PostMapping("/createUrl")
    public ResponseEntity<?>createUrl(@RequestBody URLDto urlDto){
        if(urlRepository.existsByShortUrl(urlDto.getShortUrl())){
            CustomResponse customResponse = new CustomResponse();
            customResponse.setStatus("404");
            customResponse.setError("Content Found");
            customResponse.setMessage("["+urlDto.getShortUrl()+"]"+"This shorUrl already exists please try another one???");
            return new  ResponseEntity<CustomResponse>(customResponse, HttpStatus.OK);
        }

        urlService.createUrl(urlDto);

        Response response = new Response();
        response.setId(urlDto.getId());
        response.setShortUrl(urlDto.getShortUrl());
        return new ResponseEntity<>(response,HttpStatus.OK);
        //return ResponseEntity.ok(urlService.createUrl(urlDto));
    }
@GetMapping("/info")
    public ResponseEntity<List<Info>>getInfo(){
        List<Info> info = infoRepository.findAll();
        return new ResponseEntity<List<Info>>(info,HttpStatus.OK);
    }

    @GetMapping("/short")
    public ResponseEntity<List<ShortUrl>>getShortUrl(){
        List<ShortUrl>shortUrls=shortUrlRepository.findAll();
        return new ResponseEntity<>(shortUrls,HttpStatus.OK);
    }



    //endpoint for test purpose
    @GetMapping("/milton")
    public String url(){
        return "Url ShortLener Project";
    }



    //endpoint to count shortUrl
    @GetMapping("/count")
    public ResponseEntity<?>count(){
        Long count = urlService.count();
    if(count!=0){
        CustomResponse customResponse=new CustomResponse();
        customResponse.setStatus("200");
        customResponse.setMessage("Total shortUrl is: "+count.toString());
        customResponse.setError("No error found");
        return new ResponseEntity<>(customResponse,HttpStatus.OK);
    }else{
        CustomResponse customResponse = new CustomResponse();
        customResponse.setStatus("204");
        customResponse.setMessage("Yet! There is no shortUrl ! please create some url first");
        customResponse.setError("No Content");
        return new ResponseEntity<>(customResponse,HttpStatus.OK);
    }

    }
}
