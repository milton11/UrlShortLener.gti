package com.milton.dto;

import com.milton.entity.Parameters;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class URLDto {
    private Long id;
    @NotNull
    private String shortUrl;
    @NotNull
    private String longUrl;

    private Parameters parameters;
}
