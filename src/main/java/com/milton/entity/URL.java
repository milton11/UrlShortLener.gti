package com.milton.entity;

import com.milton.dto.ParameterDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class URL {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NaturalId
    @Column(unique = true,nullable = false,length = 6)
    private String shortUrl;

    @Column(nullable = false)
    private String longUrl;

    @OneToOne(cascade = CascadeType.ALL)
    private Parameters parameters;

    public URL(String shortUrl, String longUrl, Parameters parameters) {
        this.shortUrl=shortUrl;
        this.longUrl=longUrl;
        this.parameters=parameters;
    }
}
