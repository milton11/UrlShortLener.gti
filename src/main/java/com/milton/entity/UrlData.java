package com.milton.entity;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@Builder
@Data
public class UrlData {
    private Map<String,String>data;
}
