package com.milton.repository;

import com.milton.entity.ShortUrl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShortUrlRepository extends JpaRepository<ShortUrl,Long> {
    Boolean existsByShortUrl(String shortUrl);
}
