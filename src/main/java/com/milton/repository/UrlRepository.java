package com.milton.repository;

import com.milton.entity.URL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UrlRepository extends JpaRepository<URL,Long> {
    Optional<URL>findByShortUrl(String shortUrl);
    Boolean existsByShortUrl(String shortUrl);

    @Query(value = "SELECT count(shortUrl) FROM URL")
    public Long shotUrlCount();
}
