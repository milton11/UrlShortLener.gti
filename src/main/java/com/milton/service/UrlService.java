package com.milton.service;

import com.milton.dto.CustomResponse;
import com.milton.dto.URLDto;
import com.milton.entity.Info;
import com.milton.entity.ShortUrl;
import com.milton.entity.URL;
import com.milton.exception.MyException;
import com.milton.repository.InfoRepository;
import com.milton.repository.ShortUrlRepository;
import com.milton.repository.UrlRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UrlService {
private static  final Logger logger = LoggerFactory.getLogger(UrlService.class);
    private final UrlRepository urlRepository;
    private final InfoRepository infoRepository;
    private final ShortUrlRepository shortUrlRepository;
    static int count=0;

    public Object createUrl(URLDto urlDto){

     System.out.println("Url Request:"+urlDto.toString());

     if(urlDto.getShortUrl().length()>6){
         throw new MyException("The length of this short url is greater than 6");
     }
        URL longUrl = new URL(urlDto.getShortUrl(), urlDto.getLongUrl(), urlDto.getParameters());
        URL url = urlRepository.save(longUrl);
        System.out.println("Url: " + url);
        return Optional.ofNullable(url);
    }

    public URL getUrl(String shortUrl) throws UnknownHostException {
        saveInfo();
        saveShortUrl(shortUrl);

    URL url = urlRepository.findByShortUrl(shortUrl).orElseThrow(()->
            new MyException("This is not found try another one"));
    System.out.println("Url::"+url);
        return url;
    }

    public void saveInfo() throws UnknownHostException {
        InetAddress inetAddress = InetAddress.getLocalHost();
        String ip = inetAddress.getHostAddress();
        String hostName=inetAddress.getHostName();
        Info info=new Info(ip,hostName);
        infoRepository.save(info);

    }

    public ResponseEntity<CustomResponse> saveShortUrl(String s){
    count++;
        if(shortUrlRepository.existsByShortUrl(s)){
            CustomResponse customResponse = new CustomResponse();
            customResponse.setStatus("404");
            customResponse.setError("Content Found");
            customResponse.setMessage("["+s+"]"+"duplicate  short url??");
            return new  ResponseEntity<CustomResponse>(customResponse, HttpStatus.OK);
        }
        ShortUrl shortUrl=new ShortUrl(s);
        shortUrlRepository.save(shortUrl);
        System.out.println("Count: "+count);
        return null;
    }

    public URL findById(Long id) {
        URL url = urlRepository.findById(id).orElseThrow(()->
                new MyException(id+ " is not found try another one!!!!!"));
        return url;
    }

    public List<URL> getAllUrl() {
        List<URL>url = urlRepository.findAll();
        return url;
    }





    public Long count(){
        return urlRepository.count();
    }
}
