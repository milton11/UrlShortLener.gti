package com.milton.shortUrlHandler;

import com.milton.dto.ParameterDto;
import com.milton.dto.URLDto;
import org.springframework.web.client.RestTemplate;

public class UrlClientHandler {
    private static final String base_url = "http://localhost:8080/api/url/";
    private static RestTemplate restTemplate;
    public URLDto getLongUrlByShortUrl(final String shortUrl){
        return restTemplate.getForObject(base_url+"/{shortUrl}",URLDto.class,shortUrl);
    }

    public ParameterDto addParam(String param1, String param2){
        String p = param1+param2;
        return restTemplate.getForObject(base_url+param1+param2, ParameterDto.class,p);
    }
}
